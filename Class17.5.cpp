// Class17.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << std::endl;
    }

    void vectorLength()
    {
        int l = x * x + y * y + z * z;
        std::cout << '\n' << sqrt(l) << std::endl;
    }
};

int main()
{
    std::cout << " Hello! \n I can show a vector's length, which coordinates you'll set!" <<
        "\n Here we go!" << std::endl;

    
    int a, b, c;
    std::cout << "Enter the coordinate [x]" << std::endl;
    std::cin >> a;
    std::cout << "Enter the coordinate [y]" << std::endl;
    std::cin >> b;
    std::cout << "Enter the coordinate [z]" << std::endl;
    std::cin >> c;

    Vector V(a, b, c);

    std::cout << " So, the vector's length is:" << std::endl;
    V.vectorLength();
    
}
